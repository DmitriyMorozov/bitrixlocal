<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
    return "";

$strReturn = '';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
    $title = ($arResult[$index]["TITLE"]);
    $arrow = ($index > 0? ' &gt; ' : '');

    if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
    {
        $strReturn .= '
			<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'">'.$title.$arrow.'</a>';
    }
    else
    {
        $strReturn .= '<span>'.$title.'</span>';
    }
}

return $strReturn;